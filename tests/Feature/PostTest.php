<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Post;
use App\Models\Category;

class PostTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_posts_can_have_category()
    {
        // Test that a newly created post can be assigned a category

        $user = User::factory()->create();

        $post = Post::factory()->create(['user_id' => $user->id]);
        $category = Category::factory()->create();
        $post->categories->add($category);

        $this->assertNotEmpty($post->categories);
    }

        public function test_post_can_have_categories()
    {
        // Test that posts can have multiple categoroes

        $user = User::factory()->create();

        $post = Post::factory()->create(['user_id' => $user->id]);
        $categories = Category::factory(5)->create();
        $post->categories()->saveMany($categories);

        $this->assertEquals($post->categories->count(), 5);
    }

     
     public function test_post_has_slug_from_title()
      {
        //test that when user creates a post with title the slug is set interally
            $user = User::factory()->create();

          $post = Post::factory()->create(['user_id' => $user->id]);
          $categories = Category::factory()->create();
          $post->categories->add($categories);
       
        $this->assertEquals(\Str::slug($post['title'], '-'), $post->slug);
      }

    
}

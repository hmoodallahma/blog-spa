<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Post;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_creates_user()
    {
        //Test a user has been created
        $user = User::factory()->create();
        $this->assertNotEmpty($user);
    }

    public function test_user_can_create_post()
    {
        // Test that the a newly created user can create posts
        // Test that the user's ID can be user to retrieve posts

        $user = User::factory()->create();

        $post = Post::factory()->create(['user_id' => $user->id]);

        $this->assertNotEmpty($user->posts);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	//Need to has password so it is the same when it retrieved from the db as a hash
        \App\Models\User::factory(1)->create(['email' => 'aisha@sugarcode.ca', 'password' => Hash::make('bobrules')]);
        \App\Models\User::factory(1)->create(['email' => 'bob@bob.com', 'password' => Hash::make('bobrules')]);

        \DB::table('categories')->insert([
        	['name' => 'General'],
        	['name' => 'Technology'],
        	['name' => 'Press Release'],
        	['name' => 'Culture'],
        	['name' => 'Reviews'],
        ]);
        //Create posts after categories because post factory depends on categories in db
        \App\Models\Post::factory(50)->create(['user_id' => 1]);
        

    }
}
